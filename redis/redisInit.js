import redis from "redis";

let client = redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
});

client.on("connect", function () {
  console.log("Redis Server Connected");
});

function addRefreshToken(id ,refreshToken){
    client.set( id , refreshToken , 'EX' , 24*60*60 , function(err , reply){
        if(err) return res.status(500).json(err)
    })
}



export { addRefreshToken , client }