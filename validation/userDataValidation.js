import { body } from "express-validator";

const userRegisterValidator = () => {
  return [
    body("formData.firstName")
      .notEmpty()
      .withMessage("Firstname Cannot be empty")
      .isLength({ min: 2 })
      .withMessage("Firstname should contain more than 2 characters"),
    body("formData.lastName")
      .notEmpty()
      .withMessage("Lastname Cannot be empty")
      .isLength({ min: 2 })
      .withMessage("Lastname should contain more than 2 characters"),
    body("formData.email")
      .notEmpty()
      .withMessage("Email Cannot be empty")
      .isEmail()
      .withMessage("Email should have a proper format"),
    body("formData.password")
      .notEmpty()
      .withMessage("Password Cannot be empty")
      .isLength({ min: 8 })
      .withMessage("Password should contain more than 8 characters"),
  ];
};

const userLoginValidator = () => {
  return [
    body("formData.email")
      .notEmpty()
      .withMessage("Email Cannot be empty")
      .isEmail()
      .withMessage("Email should have a proper format"),
    body("formData.password")
      .notEmpty()
      .withMessage("Password Cannot be empty")
      .isLength({ min: 8 })
      .withMessage("Password should contain more than 8 characters"),
  ];
};

export { userRegisterValidator , userLoginValidator };
