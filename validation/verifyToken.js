import jwt from "jsonwebtoken";
import { client } from "./../redis/redisInit";

const verifyToken = async (req, res, next) => {
  // Get access token from cookies
  const { accessToken } = req.cookies;
  // Validate Token
  jwt.verify(accessToken , process.env.SECRECT , function(err , data){
    if(err) return verifyRefreshToken(accessToken , req , res , next);
    next()
  })
};

const verifyRefreshToken = (accessToken , req , res , next) => {
  client.get(accessToken , function(err , data){
    if(err) return res.sendStatus(401);
    // Gnerate a new AccessToken
    const newAccessToken = jwt.sign({ id : data.id } , process.env.SECRECT , { expiresIn : process.env.ACCESS_TOKEN_LIFE })
    console.log({ accessToken , newAccessToken })
    // Store the access Token
    client.set(newAccessToken , data)
    // Delete the previous one
    client.del(accessToken);
    // Clear the invalid cooklie
    res.clearCookie(accessToken);
    // Generate a new cooie wwith valid token
    res.cookie("accessToken" , newAccessToken , {
      httpOnly : true,
      sameSite : 'lax',
      secure : process.env.PRODUCTION ? true : false
    }).sendStatus(200);
    return next()
  })
}

export { verifyToken };
