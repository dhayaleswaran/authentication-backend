import express from "express";
import bcrypt from "bcrypt";
import {
  userRegisterValidator,
  userLoginValidator,
} from "../validation/userDataValidation";
import { cookie, validationResult } from "express-validator";
import { UserModule } from "../modules/connection";
import jwt from "jsonwebtoken";
import { verifyToken } from "../validation/verifyToken";
import { addRefreshToken, client } from "../redis/redisInit";

const userRoute = express.Router();

userRoute.post("/login", userLoginValidator(), async (req, res) => {
  const errors = validationResult(req);
  const { formData } = req.body;

  // Check for errors
  if (!errors.isEmpty()) return res.status(400).json(errors);

  let dbData;
  try {
    dbData = await UserModule.findOne({
      where: {
        email: formData.email,
      },
    });
  } catch (err) {
    if (err) return res.status(500).json({ msg: err });
  }

  // Check for PasswordMatches
  const isMatched = await bcrypt.compare(formData.password, dbData.password);

  if (!isMatched) return res.status(401).json({ msg: "Unauthorized" });
  // Issue Token
  const token = jwt.sign({ id: dbData.id }, process.env.SECRECT, {
    expiresIn: process.env.ACCESS_TOKEN_LIFE,
  });
  // Refersh Token
  const refreshToken = jwt.sign({ id: dbData.id }, process.env.REFRESH_TOKEN, {
    expiresIn: process.env.REFRESH_TOKEN_LIFE,
  });

  let userid = dbData.id;
  addRefreshToken(token, refreshToken);

  // Add token to resp Cookie
  res.cookie("accessToken", token, {
    httpOnly: true,
    sameSite: "lax",
    maxAge: 1000 * 60 * 60 * 24,
    secure: process.env.PRODUCTION ? true : false,
  }).status(200).json({
    msg: "logged In",
  });
});

userRoute.post("/register", userRegisterValidator(), async (req, res) => {
  // Store the errors in Validation
  const errors = validationResult(req);
  let data = null;

  // Check for errors
  if (!errors.isEmpty()) return res.status(400).json(errors);

  // Req.data destructuring
  const { formData } = req.body;
  // Salt Rounds
  let rounds = 10;
  // Generate Salt
  let salt = await bcrypt.genSalt(rounds);
  // hash plainext password
  const hashedPassword = await bcrypt.hash(formData.password, salt);
  // Delete plaintext password from formData Obj
  delete formData.password;
  // Add hashed password to formData Obj
  formData.password = hashedPassword;
  try {
    data = await UserModule.create(formData);
  } catch (err) {
    console.log(err);
    if (err) return res.status(500).json({ msg: "Unable to Create User" });
  }
  // Issue Token
  const token = jwt.sign({ id: data.id }, process.env.SECRECT, {
    expiresIn: process.env.ACCESS_TOKEN_LIFE,
  });
  // Refersh Token
  const refreshToken = jwt.sign({ id: data.id }, process.env.REFRESH_TOKEN, {
    expiresIn: process.env.REFRESH_TOKEN_LIFE,
  });

  addRefreshToken(token, refreshToken);
  // Add token to resp Cookie
  res.cookie("accessToken", token, {
    httpOnly: true,
    sameSite: "lax",
    secure: process.env.PRODUCTION,
  }).status(200).json({ msg: "Account Created Successfully" });
});

userRoute.get("/home", verifyToken, async (req, res) => {
  try {
    await UserModule.findAll().then((result) => res.status(200).json(result));
  } catch (err) {
    if (err) return res.status(400).send(err);
  }
});

userRoute.get("/logout", (req, res) => {
  const { refreshToken } = req.cookies;
  res.clearCookie(refreshToken);
});

export { userRoute };
