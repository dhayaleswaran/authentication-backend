import express from 'express';
import { } from 'dotenv/config';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { userRoute } from './routes/userRoute';


const app = express();

app.use(cors())
app.use(express.json());
app.use(cookieParser())
app.use("/users" , userRoute);


app.listen(process.env.PORT , console.log(`I'm running at ${process.env.PORT}`))