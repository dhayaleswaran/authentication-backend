import { Sequelize } from "sequelize";
import { userDefinition } from './userModule';


const { DATABASE, DB_USERNAME, PASSWORD, DIALECT, HOST } = process.env;


const connection = new Sequelize(DATABASE, DB_USERNAME, PASSWORD, {
  host: HOST,
  dialect: DIALECT,
});

const UserModule = userDefinition(connection);

connection.sync();

export { UserModule }